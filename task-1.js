const task1 = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const app = task1();
const password = "password";

const port = 8000;

app.get('/', (req, res) => {
    res.send("Hello, task 1");
});


app.get('/:title', (req, res) => {
    res.send(req.params.title);
});


app.get('/encode/:password', (req, res) => {
    res.send(Vigenere.Cipher('password').crypt(req.params.password));
});


app.get('/decode/:password', (req, res) => {
    res.send(Vigenere.Decipher('password').crypt(req.params.password));
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});